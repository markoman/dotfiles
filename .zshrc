# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000
bindkey -e
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename '/home/marko/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall
#

alias ls="lsd"
alias ll="lsd -la"

alias update="sudo pacman -Syyu"
alias install="sudo pacman -S"
alias remove="sudo pacman -R"
alias search="sudo pacman -Ss"

clear;neofetch


# start starship
eval "$(starship init zsh)"
